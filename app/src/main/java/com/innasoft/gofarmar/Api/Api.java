package com.innasoft.gofarmar.Api;

import com.innasoft.gofarmar.Response.LoginMobileResponse;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface Api {


    //login with mobile number

    @FormUrlEncoded
    @POST("login-otp")
    Call<LoginMobileResponse> LoginMobile(@Field("mobile") String mobile,
                                          @Header("X-Deviceid") String brower_id);


}
