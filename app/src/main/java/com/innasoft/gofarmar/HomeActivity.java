package com.innasoft.gofarmar;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.innasoft.gofarmar.Adapter.Adapter;
import com.innasoft.gofarmar.Response.HomeItem;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.homeRecycler)
    RecyclerView homeRecycler;
    @BindView(R.id.fab)
    FloatingActionButton fab;

    Adapter adapter ;

    List<HomeItem> homeItemList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL,false);
        homeRecycler.setLayoutManager(mLayoutManager);
        adapter = new Adapter(homeItemList,this);
        homeRecycler.setAdapter(adapter);

        homeData();


    }

    private void homeData(){
        HomeItem homeItem = new HomeItem("Tamato","1000","https://i.ytimg.com/vi/jGGk2FZ5zSs/hqdefault.jpg","100kgs","vegitables","Organic Farming","Yes");
        homeItemList.add(homeItem);

        homeItem = new HomeItem("Potato","2000","https://cdn-prod.medicalnewstoday.com/content/images/articles/280/280579/potatoes-can-be-healthful.jpg","100kgs","vegitables","Organic Farming","Yes");
        homeItemList.add(homeItem);

        homeItem = new HomeItem("Onions","5000","https://img.etimg.com/thumb/width-640,height-480,imgsize-876498,resizemode-1,msid-67172581/despite-scheme-onions-plunge-to-rs-1-50-in-maharashtra.jpg","100kgs","vegitables","Organic Farming","Yes");
        homeItemList.add(homeItem);

        homeItem = new HomeItem("Brinjal","1000","https://countercurrents.org/wp-content/uploads/2019/04/Bt-Brinjal.jpg","100kgs","vegitables","Organic Farming","Yes");
        homeItemList.add(homeItem);


        homeItem = new HomeItem("Cauliflower","3000","https://i0.wp.com/images-prod.healthline.com/hlcmsresource/images/AN_images/cauliflower-thumb.jpg?w=756&h=567","100kgs","vegitables","Organic Farming","Yes");
        homeItemList.add(homeItem);

        homeItem = new HomeItem("Apple","3000","https://ichef.bbci.co.uk/wwfeatures/live/976_549/images/live/p0/7v/2w/p07v2wjn.jpg","50kgs","Fruits","Organic Farming","Yes");
        homeItemList.add(homeItem);
    }

    @OnClick(R.id.fab)
    public void onViewClicked() {
        Intent intent = new Intent(HomeActivity.this, AddProductActivity.class);
        startActivity(intent);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()){
            case  R.id.itemprofile:
                Intent intent = new Intent(HomeActivity.this, ProfileActivity.class);
                startActivity(intent);
                break;

        }
        return super.onOptionsItemSelected(item);
    }
}
