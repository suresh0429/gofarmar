package com.innasoft.gofarmar;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import in.aabhasjindal.otptextview.OtpTextView;

public class OtpActivity extends Activity {

    @BindView(R.id.logo_img)
    ImageView logoImg;
    @BindView(R.id.textView)
    TextView textView;
    @BindView(R.id.otp_view)
    OtpTextView otpView;
    @BindView(R.id.verify_btn)
    Button verifyBtn;
    @BindView(R.id.txtresend)
    TextView txtresend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.verify_btn, R.id.txtresend})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.verify_btn:
                Intent i = new Intent(OtpActivity.this, HomeActivity.class);
                startActivity(i);

                break;
            case R.id.txtresend:
                break;
        }
    }
}
