package com.innasoft.gofarmar;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;
import com.innasoft.gofarmar.Api.RetrofitClient;
import com.innasoft.gofarmar.Response.LoginMobileResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends Activity {


    @BindView(R.id.edt_mobile)
    TextInputEditText edtMobile;
    @BindView(R.id.btn_continue)
    Button btnContinue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);

        btnContinue.setAlpha(.5f);
        btnContinue.setEnabled(false);
        edtMobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String value = String.valueOf(charSequence.length());

                if (value.equals("10")) {

                    btnContinue.setEnabled(true);
                    btnContinue.setAlpha(.9f);
                    btnContinue.setText("CONTINUE");
                    btnContinue.setClickable(true);
                } else {
                    btnContinue.setAlpha(.5f);
                    btnContinue.setEnabled(false);
                    btnContinue.setText("ENTER MOBILE NUMBER");
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    @OnClick(R.id.btn_continue)
    public void onViewClicked() {

        Intent intent = new Intent(LoginActivity.this, OtpActivity.class);
        startActivity(intent);
        finish();
    }
    
    void login() {
        final String Mobile = edtMobile.getText().toString().trim();
        if (Mobile.equals("")) {
            Toast.makeText(LoginActivity.this, "Enter Mobile Number...", Toast.LENGTH_SHORT).show();
        } else {

            final ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
            progressDialog.setMessage("Loading....");
            progressDialog.show();
            // Call<LoginMobileResponse> call = RetrofitClient.getInstance().getApi().LoginMobile(Mobile, deviceId);
            Call<LoginMobileResponse> call = RetrofitClient.getInstance().getApi().LoginMobile(Mobile, "");
            call.enqueue(new Callback<LoginMobileResponse>() {
                @Override
                public void onResponse(Call<LoginMobileResponse> call, Response<LoginMobileResponse> response) {
                    if (response.isSuccessful()) ;
                    LoginMobileResponse loginMobileResponse = response.body();
                    if (loginMobileResponse.getStatus().equals("10100")) {
                        progressDialog.dismiss();
                        //progress_indicator.stop();
                        Toast.makeText(LoginActivity.this, loginMobileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(LoginActivity.this, OtpActivity.class);
                        intent.putExtra("Mobile_number", Mobile);
                        startActivity(intent);

                        //setDefaults();

                    } else if (loginMobileResponse.getStatus().equals("10200")) {
                        progressDialog.dismiss();
                        //progress_indicator.stop();
                        Toast.makeText(LoginActivity.this, loginMobileResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    } else if (loginMobileResponse.getStatus().equals("10300")) {
                        progressDialog.dismiss();
                        //Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                        // intent.putExtra("Mobile_number", Mobile);
                        // startActivity(intent);
                        // Toast.makeText(LoginActivity.this, loginMobileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    } else if (loginMobileResponse.getStatus().equals("10400")) {
                        progressDialog.dismiss();
                        Toast.makeText(LoginActivity.this, loginMobileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }

                @Override
                public void onFailure(Call<LoginMobileResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    //progress_indicator.stop();
                    Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        }
    }


}
