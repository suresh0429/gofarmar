package com.innasoft.gofarmar.Response;

public class HomeItem {

    private String productName;
    private String price;
    private String image;
    private String weight;
    private String category;
    private String type_of_Farming;
    private String transport_Facility;

    public HomeItem(String productName, String price, String image, String weight, String category, String type_of_Farming, String transport_Facility) {
        this.productName = productName;
        this.price = price;

        this.image = image;
        this.weight = weight;
        this.category = category;
        this.type_of_Farming = type_of_Farming;
        this.transport_Facility = transport_Facility;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }


    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getType_of_Farming() {
        return type_of_Farming;
    }

    public void setType_of_Farming(String type_of_Farming) {
        this.type_of_Farming = type_of_Farming;
    }

    public String getTransport_Facility() {
        return transport_Facility;
    }

    public void setTransport_Facility(String transport_Facility) {
        this.transport_Facility = transport_Facility;
    }
}
