package com.innasoft.gofarmar.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;

import com.innasoft.gofarmar.R;

public class SingleChoiceDialogFragment extends DialogFragment {

    int position = 0; //default selected position

    public interface SingleChoiceListener {
        void onPositiveButtonClicked(String[] list, int position);

        void onNegativeButtonClicked();
    }

    SingleChoiceListener mListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (SingleChoiceListener) context;
        } catch (Exception e) {
            throw new ClassCastException(getActivity().toString() + " SingleChoiceListener must implemented");
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final String[] list = {"Fruits", "Vegetables", "Millets","Grocery"};

        builder.setTitle("Select Category")
                .setSingleChoiceItems(list, position, (dialogInterface, i) -> position = i)
                .setPositiveButton("Ok", (dialogInterface, i) -> mListener.onPositiveButtonClicked(list, position))
                .setNegativeButton("Cancel", (dialogInterface, i) -> mListener.onNegativeButtonClicked());

        return builder.create();
    }
}
