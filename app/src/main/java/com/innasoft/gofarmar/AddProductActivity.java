package com.innasoft.gofarmar;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.DialogFragment;

import com.google.android.material.textfield.TextInputEditText;
import com.innasoft.gofarmar.Fragments.SingleChoiceDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddProductActivity extends AppCompatActivity implements SingleChoiceDialogFragment.SingleChoiceListener {

    private static final String TAG = "AddProductActivity";
    @BindView(R.id.etCategory)
    TextInputEditText etCategory;
    @BindView(R.id.etProduct)
    TextInputEditText etProduct;
    @BindView(R.id.etWeight)
    TextInputEditText etWeight;
    @BindView(R.id.etProductPrice)
    TextInputEditText etProductPrice;
    @BindView(R.id.rb1)
    RadioButton rb1;
    @BindView(R.id.rb2)
    RadioButton rb2;
    @BindView(R.id.rbYes)
    RadioButton rbYes;
    @BindView(R.id.rbNo)
    RadioButton rbNo;
    @BindView(R.id.txtStatus)
    TextView txtStatus;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;
    @BindView(R.id.rbTypeforming)
    RadioGroup rbTypeforming;
    @BindView(R.id.tfRadiogroup)
    RadioGroup tfRadiogroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Add Product");

        tfRadiogroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton rb=(RadioButton)findViewById(i);

                if (rb.getText().equals("Yes")){
                    txtStatus.setVisibility(View.VISIBLE);
                }else {
                    txtStatus.setVisibility(View.GONE);
                }
                Toast.makeText(getApplicationContext(), rb.getText(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onCheckedChanged: "+rb);
            }
        });

        rbTypeforming.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                RadioButton rb=(RadioButton)findViewById(i);
                Toast.makeText(getApplicationContext(), rb.getText(), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "onCheckedChanged: "+rb);
            }
        });


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick({R.id.etCategory, R.id.btnSubmit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.etCategory:
                DialogFragment singleChoiceDialog = new SingleChoiceDialogFragment();
                singleChoiceDialog.setCancelable(false);
                singleChoiceDialog.show(getSupportFragmentManager(), "Single Choice Dialog");
                break;
            case R.id.btnSubmit:
                break;
        }
    }

    @Override
    public void onPositiveButtonClicked(String[] list, int position) {
        etCategory.setText("" + list[position]);
    }

    @Override
    public void onNegativeButtonClicked() {

    }
}
