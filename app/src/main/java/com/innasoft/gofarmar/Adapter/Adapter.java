package com.innasoft.gofarmar.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.innasoft.gofarmar.R;
import com.innasoft.gofarmar.Response.HomeItem;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class Adapter extends RecyclerView.Adapter<Adapter.MyViewHolder> {
    List<HomeItem> model;
    Context context;



    public Adapter(List<HomeItem> model, Context context) {
        this.model = model;
        this.context = context;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imagProduct)
        ImageView imagProduct;
        @BindView(R.id.txtProduct)
        TextView txtProduct;
        @BindView(R.id.txtWeight)
        TextView txtWeight;
        @BindView(R.id.txtPrice)
        TextView txtPrice;
        @BindView(R.id.txtType)
        TextView txtType;
        @BindView(R.id.txtFacility)
        TextView txtFacility;
        @BindView(R.id.txtCategoery)
        TextView txtCategoery;

        public MyViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);


        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Glide.with(context).load(model.get(position).getImage()).centerCrop().into(holder.imagProduct);
        Log.d(TAG, "onBindViewHolder: " + model.get(position).getImage());
        holder.txtProduct.setText(model.get(position).getProductName());
        holder.txtPrice.setText("\u20B9" + model.get(position).getPrice());
        holder.txtType.setText("Type of Farming : "+model.get(position).getType_of_Farming());
        holder.txtFacility.setText("Transport Facility : "+model.get(position).getTransport_Facility());
        holder.txtWeight.setText(model.get(position).getWeight());
        holder.txtCategoery.setText(model.get(position).getCategory());
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return model.size();

    }
}
